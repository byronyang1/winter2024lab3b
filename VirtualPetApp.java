import java.util.Scanner;
public class VirtualPetApp {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Panda[] embarrassment = new Panda[4];
		for (int i = 0; i < embarrassment.length; i++) {
			embarrassment[i] = new Panda();
			System.out.println("What is the name of the " + (i+1) + "st panda: ");
			embarrassment[i].name = reader.nextLine();
			System.out.println("How much does " + embarrassment[i].name + " weight (in kg): ");
			embarrassment[i].weight = Double.parseDouble(reader.nextLine());
			System.out.println("How many hours does " + embarrassment[i].name + " sleep in average in a day: ");
			embarrassment[i].hoursSleep = Integer.parseInt(reader.nextLine());
		}
		System.out.println(embarrassment[embarrassment.length-1].name);
		System.out.println(embarrassment[embarrassment.length-1].weight);
		System.out.println(embarrassment[embarrassment.length-1].hoursSleep); 
		embarrassment[0].eatBamboos();
		embarrassment[0].sleep();
	}
}