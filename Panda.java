public class Panda {
	public String name;
	public double weight;
	public int hoursSleep;
	
	public void eatBamboos() {
		if (this.weight >= 120) {
			System.out.println(name + " you are overweight! You should eat less bamboos!");
		} else {
			System.out.println(name + " you are underweight! Please eat more bamboos!"); 
		}
	}
	
	public void sleep() {
		if (this.hoursSleep >= 10) {
			System.out.println(name + " you are sleeping too much! Go do something productive!");
		} else {
			System.out.println(name + " you are not sleeping enough! Pandas need around 10 hours of sleep per day!");
		}
	}
}